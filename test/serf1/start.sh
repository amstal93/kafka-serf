#!/bin/sh

DIR=$(dirname $0)

# chroot
cd $DIR

export SERFBIN="${SERFBIN:-$DIR/serf}"
export SERFDIR="${SERFDIR:-$DIR}"
export SERFLOG="${SERFLOG:-/dev/stdout}"

# Run Kafka
exec 2>&1
exec 1> $SERFLOG
exec $SERFBIN agent -config-dir=./conf.d
